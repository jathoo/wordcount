package wordcount.input

import wordcount.application.ApplicationHandler

import static wordcount.utils.UserMessageUtils.FILE_NOT_FOUND

class TextFileReader {
    static Closure<List<String>> readFileByLine = { String fileNameWithPath ->
        try {
            new File(fileNameWithPath).readLines()
        } catch (FileNotFoundException) {
            ApplicationHandler.runApplication FILE_NOT_FOUND
        }
    }
}
