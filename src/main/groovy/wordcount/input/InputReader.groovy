package wordcount.input

class InputReader {

    static Closure<String> getUserInformation = { String displayMessage -> println displayMessage; System.in.newReader().readLine() }
}
