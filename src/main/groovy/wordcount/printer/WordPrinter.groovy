package wordcount.printer


import static wordcount.utils.UserMessageUtils.LINE
import static wordcount.application.ApplicationHandler.setApplicationStatus

class WordPrinter {
    static void executeUserView(Map wordCount) { displayResults(wordCount); setApplicationStatus() }

    static void displayResults(Map wordCount) { wordCount?.each { k, v -> println "$LINE ${k} - ${v}" } }
}
