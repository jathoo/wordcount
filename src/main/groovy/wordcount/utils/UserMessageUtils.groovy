package wordcount.utils

class UserMessageUtils {
    public static String ENTER_FILE_NAME = "Please Enter a file name (Eg:src/test/resources/SampleTestFile1)"
    public static String ENTER_C_TO_PLAY_AGAIN = "\n Type 'c' and Press Enter to check another file or press 'Enter' to exit"
    public static String LINE = "Line"
    public static String FILE_NOT_FOUND = "File not Found - $ENTER_FILE_NAME"
 }
