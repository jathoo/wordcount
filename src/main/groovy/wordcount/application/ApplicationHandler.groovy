package wordcount.application


import static wordcount.utils.UserMessageUtils.ENTER_C_TO_PLAY_AGAIN
import static wordcount.utils.UserMessageUtils.ENTER_FILE_NAME
import static wordcount.calculator.WordCountManager.calculateWords
import static wordcount.input.InputReader.getUserInformation
import static wordcount.printer.WordPrinter.executeUserView

class ApplicationHandler {

    static runApplication = { executeUserView calculateWords(it) }

    static setApplicationStatus() { continueApplication() ? runApplication(ENTER_FILE_NAME) : stopApplication }

    static boolean continueApplication() { getUserInformation(ENTER_C_TO_PLAY_AGAIN) == 'c' }

    static stopApplication = { System.exit(0) }
}
