package wordcount.calculator


import static wordcount.calculator.WordCalculator.createWordCountPerLine
import static wordcount.input.InputReader.getUserInformation
import static wordcount.input.TextFileReader.readFileByLine

class WordCountManager {

    public static Closure<Map> calculateWords = getUserInformation >> readFileByLine >> createWordCountPerLine

}
