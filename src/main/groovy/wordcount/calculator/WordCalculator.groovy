package wordcount.calculator

class WordCalculator {

    static Closure<Map> createWordCountPerLine = { List<String> wordsPerLine ->
        wordsPerLine?.withIndex()?.collectEntries { String word, Integer index -> [(index + 1): createSortedWordCount(word)] }
    }

    static Map createSortedWordCount(String wordToCount) {
        wordToCount
                .split()
                .inject([:]) { Map wordCountMap, String word -> wordCountMap[word] = 1.plus wordCountMap[word] ?: 0; wordCountMap }
                .sort { -it.value }
    }
}
