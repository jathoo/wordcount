import static wordcount.utils.UserMessageUtils.ENTER_FILE_NAME
import static wordcount.application.ApplicationHandler.runApplication

class ApplicationStarter {
    static void main(String[] args) {
        runApplication ENTER_FILE_NAME
    }
}
