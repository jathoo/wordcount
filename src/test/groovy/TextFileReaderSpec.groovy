import spock.lang.Specification
import spock.lang.Unroll

import static wordcount.input.TextFileReader.readFileByLine

class TextFileReaderSpec extends Specification {
    public static String SAMPLE_TEST_FILE = "src/test/resources/SampleTestFile"

    @Unroll("#fileName should return list of strings per line")
    def "should read file by file name"() {
        when:
        List<String> result = readFileByLine(fileName)

        then:
        result == expected

        where:
        fileName               | expected
        "${SAMPLE_TEST_FILE}1" | ["aa bb cc", "a b c", "new"]
        "${SAMPLE_TEST_FILE}2" | ["this is a test", "this is second line test"]
    }
}
