import wordcount.calculator.WordCalculator
import wordcount.printer.WordPrinter
import spock.lang.Specification
import spock.lang.Unroll

import static wordcount.input.TextFileReader.readFileByLine

class ApplicationSpec extends Specification {
    public static String SAMPLE_TEST_FILE = "src/test/resources/SampleTestFile"


    @Unroll("#fileName words are sorted based on the word count")
    def "should read words from a file and sort it based on the word count"() {
        given:
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        System.out = new PrintStream(outputStream)

        when:
        List<String> stringList = readFileByLine(fileName)

        and:
        def map = WordCalculator.createWordCountPerLine(stringList)
        WordPrinter.displayResults(map)

        then:
        outputStream.toString() == expected

        where:
        fileName               | expected
        "${SAMPLE_TEST_FILE}3" | "Line 1 - [cat:3, dog:2]\nLine 2 - [dog:3, cat:2]\n"
        "${SAMPLE_TEST_FILE}4" | "Line 1 - [cat:3, dog:2]\nLine 2 - [dog:3, cat:2]\n"
        "${SAMPLE_TEST_FILE}1" | "Line 1 - [aa:1, bb:1, cc:1]\nLine 2 - [a:1, b:1, c:1]\nLine 3 - [new:1]\n"
    }

}
