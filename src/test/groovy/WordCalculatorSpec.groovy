import wordcount.calculator.WordCalculator
import spock.lang.Specification

class WordCalculatorSpec extends Specification {

    def "should return a map of index and list of words with word count"() {
        given:
        Map one = [badman: 5, superMan: 4, something: 3, somethingElse: 40]
        Map two = [Rose: 10, Jazz: 4]
        Map three = [one: 1, two: 3]

        List<String> stringList = [createWord(one), createWord(two), createWord(three)]

        when:
        def result = WordCalculator.createWordCountPerLine(stringList)

        then:
        result == [1: one.sort { -it.value }, 2: two.sort { -it.value }, 3: three.sort { -it.value }]
    }

    private static String createWord(Map<String, Integer> NumberOfWords) {
        String word = ""
        NumberOfWords.each { key, value -> value.times { word = "$word ${key}" } }
        word
    }
}
