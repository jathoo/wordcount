import wordcount.printer.WordPrinter
import spock.lang.Specification

class WordPrinterSpec extends Specification {

    def "prints details map"() {
        given:
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        System.out = new PrintStream(outputStream)

        when:
        Map map = [1: [somethingElse: 40, badman: 5, superMan: 4, something: 3], 2: [Rose: 10, Jazz: 4]]
        WordPrinter.displayResults(map)

        then:
        outputStream.toString() == "Line 1 - [somethingElse:40, badman:5, superMan:4, something:3]\nLine 2 - [Rose:10, Jazz:4]\n"
    }
}
