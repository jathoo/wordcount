## Assumptions

1. System will work with any plain text file without formatting (eg: tested with txt and non formatted text files).
2. words are separated by space.
3. Empty Lines before EOF will be counted as lines with empty map.


## Prerequisites

1. gradle 
2. Java 8

This project is using Java 8 and gradle

### running the application 

#### command prompt 
1. To run the application use 
./gradlew run 

2. Wait for this message
"Please Enter a file name (Eg:src/test/resources/SampleTestFile1)"

3. Enter a file name with path 
Eg: src/test/resources/SampleTestFile3
    
4. result should be printed on console

#### IntelliJ
go to ApplicationStarter.class to run the application and use the program from intelliJ terminal

### to run all the test use 
./gradlew test

#### Troubleshooting 
if IntelliJ is missing the dependencies 
1. go to file
2. click re-import gradle project
